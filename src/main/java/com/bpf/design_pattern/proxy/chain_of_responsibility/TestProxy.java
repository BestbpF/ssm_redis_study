package com.bpf.design_pattern.proxy.chain_of_responsibility;

import com.bpf.design_pattern.proxy.interceptor.InterceptorJdkProxy;
import com.bpf.design_pattern.proxy.jdkProxy.HelloWorld;
import com.bpf.design_pattern.proxy.jdkProxy.HelloWorldImpl;

public class TestProxy {
    public static void main(String[] args) {
        HelloWorld proxy1 = (HelloWorld) InterceptorJdkProxy.bind(new HelloWorldImpl(),
                "com.bpf.design_pattern.proxy.chain_of_responsibility.Interceptor1");

        HelloWorld proxy2 = (HelloWorld) InterceptorJdkProxy.bind(proxy1,
                "com.bpf.design_pattern.proxy.chain_of_responsibility.Interceptor2");

        HelloWorld proxy3 = (HelloWorld) InterceptorJdkProxy.bind(proxy2,
                "com.bpf.design_pattern.proxy.chain_of_responsibility.Interceptor3");

        proxy3.sayHelloWorld();
    }
}
