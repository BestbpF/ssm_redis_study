package com.bpf.design_pattern.proxy.jdkProxy;

public interface HelloWorld {

    void sayHelloWorld();
}
