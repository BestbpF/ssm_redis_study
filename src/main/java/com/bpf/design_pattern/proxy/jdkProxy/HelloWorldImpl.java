package com.bpf.design_pattern.proxy.jdkProxy;

public class HelloWorldImpl implements HelloWorld {
    @Override
    public void sayHelloWorld() {
        System.out.println("hello world!");
    }
}
