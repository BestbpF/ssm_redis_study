package com.bpf.design_pattern.proxy.jdkProxy;

public class TestJdkProxy {

    public static void main(String[] args) {
        JdkProxyExample jdk = new JdkProxyExample();
        //绑定关系
        HelloWorld proxy = (HelloWorld) jdk.bind(new HelloWorldImpl());
        proxy.sayHelloWorld();
    }
}
