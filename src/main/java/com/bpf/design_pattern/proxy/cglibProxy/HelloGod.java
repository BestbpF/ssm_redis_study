package com.bpf.design_pattern.proxy.cglibProxy;

public class HelloGod {
    private String name;

    public HelloGod(String name){
        this.name = name;
    }

    public void helloGod(){
        System.out.println("hello " + name);
    }
}
