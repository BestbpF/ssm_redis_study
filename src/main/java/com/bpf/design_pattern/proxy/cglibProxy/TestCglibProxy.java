package com.bpf.design_pattern.proxy.cglibProxy;

public class TestCglibProxy {

    public static void main(String[] args) {
        CglibProxyExample cglibProxyExample = new CglibProxyExample();
        Class[] argumentTypes = new Class[]{String.class};
        Object[] arguments = new Object[]{"baipengfei"};
        HelloGod helloGod = (HelloGod) cglibProxyExample.getProxy(HelloGod.class, argumentTypes ,arguments);
        helloGod.helloGod();
    }
}
