package com.bpf.design_pattern.proxy.interceptor;

import java.lang.reflect.Method;

public class MyInterceptor implements Interceptor {
    @Override
    public boolean before(Object proxy, Object target, Method method, Object[] args) {
        System.err.println("反射方法前逻辑 --- 判断用户是否处于登录状态 --- 用户未登录，操作拦截");
        return false;//不反射被代理对象原有方法
    }

    @Override
    public void around(Object proxy, Object target, Method method, Object[] args) {
        System.err.println("取代了被代理对象的方法 --- 页面转发到登录页面");
    }

    @Override
    public void after(Object proxy, Object target, Method method, Object[] args) {
        System.err.println("反射方法后的逻辑 --- 记录本次异常操作");
    }
}
