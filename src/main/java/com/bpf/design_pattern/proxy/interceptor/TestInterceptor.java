package com.bpf.design_pattern.proxy.interceptor;

import com.bpf.design_pattern.proxy.jdkProxy.HelloWorld;
import com.bpf.design_pattern.proxy.jdkProxy.HelloWorldImpl;

public class TestInterceptor {

    public static void main(String[] args) {
        HelloWorld proxy1 = (HelloWorld) InterceptorJdkProxy.bind(new HelloWorldImpl(),
                "com.bpf.design_pattern.proxy.interceptor.MyInterceptor");

        HelloWorld proxy2 = (HelloWorld) InterceptorJdkProxy.bind(new HelloWorldImpl(), null);

        proxy1.sayHelloWorld();
        System.out.println("----------------------------");
        System.out.println("1111111111111111111111111");
        proxy2.sayHelloWorld();
    }


}
