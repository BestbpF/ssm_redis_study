package com.bpf.design_pattern.abstractFactory;

public class Test {
    public static void main(String[] args) {

        ProductFactory productFactory = new ProductFactory();
        productFactory.createProduct("A1");//生产一个苹果
        productFactory.createProduct("A2");//生产一个鸭梨
        productFactory.createProduct("B1");//生产了一支笔
        productFactory.createProduct("B2");//生产了一个橡皮

    }
}
