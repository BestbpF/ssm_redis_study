package com.bpf.design_pattern.abstractFactory;

/**
 * 抽象工厂和具体工厂都要实现本接口
 */
public interface IProductFactory {
    public IProduct createProduct(String productNo);
}
