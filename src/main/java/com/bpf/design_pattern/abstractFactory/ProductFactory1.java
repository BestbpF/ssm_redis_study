package com.bpf.design_pattern.abstractFactory;

public class ProductFactory1 implements IProductFactory {
    /**
     * @param productNo 产品编号
     * @return 本具体工厂只负责生产水果，水果编号都以A开头
     */
    @Override
    public IProduct createProduct(String productNo) {
        //这里不提供IProduct的实现类，以打印文字的方法简略表示
        if("A1".equals(productNo)){
            System.out.println("生产一个苹果");
        }else if ("A2".equals(productNo)){
            System.out.println("生产一个鸭梨");
        }else {
            System.out.println("水果工厂暂不生产该水果");
        }

        return null;
    }
}
