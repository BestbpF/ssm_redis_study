package com.bpf.design_pattern.abstractFactory;

/**
 * 本具体工厂只生产文具,产品编号以B开头
 */
public class ProductFactory2 implements IProductFactory {
    @Override
    public IProduct createProduct(String productNo) {
        if("B1".equals(productNo)){
            System.out.println("生产了一支笔");
        }else if ("B2".equals(productNo)){
            System.out.println("生产了一个橡皮");
        }else {
            System.out.println("本文具工厂暂不生产该文具");
        }
        return null;
    }
}
