package com.bpf.design_pattern.abstractFactory;

/**
 * 抽象工厂，负责制定某个具体工厂生产某件产品
 */
public class ProductFactory implements IProductFactory {

    public IProduct createProduct(String productNo) {
        char type = productNo.charAt(0);
        IProductFactory factory = null;

        if(type == 'A'){
            factory = new ProductFactory1();
        }else if(type == 'B'){
            factory = new ProductFactory2();
        }

        if(factory != null) factory.createProduct(productNo);

        return null;
    }
}
