package com.bpf.design_pattern.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ProductList extends Observable {

    private List<String> productList = null;//产品列表

    private static ProductList instance;//唯一实例

    private ProductList(){} //构建方法私有化，防止使用者去new对象

    /**
     * @return 产品列表唯一实例
     */
    public static ProductList getInstance(){
        if(instance == null){
            instance = new ProductList();
            instance.productList = new ArrayList<>();
        }

        return instance;
    }


    /**
     * 增加观察者(电商接口)
     * @param observer 观察者
     */
    public void addProductListObserver(Observer observer){
        this.addObserver(observer);
    }


    /**
     * 核心逻辑 增加产品就会通知已经订阅的观察者
     * @param newProduct 新产品
     */
    public void addProduct(String newProduct){
        productList.add(newProduct);
        System.out.println("产品列表新增产品：" + newProduct + "，自动通知各大电商！");
        this.setChanged();//被观察者发生变化
        this.notifyObservers(newProduct);//通知观察者(各大电商)，并传递新产品
    }
}
