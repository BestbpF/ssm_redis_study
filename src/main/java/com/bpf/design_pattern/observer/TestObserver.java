package com.bpf.design_pattern.observer;

public class TestObserver {
    public static void main(String[] args) {
        ProductList productList = ProductList.getInstance();
        productList.addProductListObserver(new JingDongObserver());
        productList.addProductListObserver(new TaoBaoObserver());

        productList.addProduct("Java编程思想");
    }
}
